package com.yuercom.jaxb1;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

public class JAXBExample1 {
	public static void main(String[] args) {
		Student1 s = new Student1();
		s.setId(1);
		s.setName("张三");
		s.setAge(25);
//		File file = new File("C:\\Users\\LINLEI\\Desktop\\123\\student.xml");
		try {
			JAXBContext jaxb = JAXBContext.newInstance(Student1.class);
			Marshaller marshaller = jaxb.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//			marshaller.marshal(s, file);
			StringWriter writer = new StringWriter();
			marshaller.marshal(s, writer);
			System.out.println(writer.toString());
			
		} catch (Exception e) {
			
		}
		
	}
	
	
	
	
	public static Student1 getStudent(){
		Student1 s = new Student1();
		s.setId(1);
		s.setName("����");
		s.setAge(25);
		return s;
	}
}
